﻿namespace Task19
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridViewBDBrowser = new System.Windows.Forms.DataGridView();
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonStudentCreate = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonUpdate = new System.Windows.Forms.Button();
            this.textBoxStudentName = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.labelStudentName = new System.Windows.Forms.Label();
            this.groupBoxStudentCreation = new System.Windows.Forms.GroupBox();
            this.dataGridViewStudents = new System.Windows.Forms.DataGridView();
            this.labelGridSuervisor = new System.Windows.Forms.Label();
            this.labelGridStudent = new System.Windows.Forms.Label();
            this.buttonToJSON = new System.Windows.Forms.Button();
            this.richTextBoxJSON = new System.Windows.Forms.RichTextBox();
            this.supervisorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBDBrowser)).BeginInit();
            this.groupBoxStudentCreation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.supervisorBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridViewBDBrowser
            // 
            this.dataGridViewBDBrowser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBDBrowser.Location = new System.Drawing.Point(412, 56);
            this.dataGridViewBDBrowser.Name = "dataGridViewBDBrowser";
            this.dataGridViewBDBrowser.RowHeadersWidth = 62;
            this.dataGridViewBDBrowser.RowTemplate.Height = 28;
            this.dataGridViewBDBrowser.Size = new System.Drawing.Size(366, 274);
            this.dataGridViewBDBrowser.TabIndex = 0;
            this.dataGridViewBDBrowser.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewBDBrowser_CellClick);
            this.dataGridViewBDBrowser.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridView1_CellContentClick);
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(1064, 351);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(146, 58);
            this.buttonRead.TabIndex = 1;
            this.buttonRead.Text = "Read";
            this.buttonRead.UseVisualStyleBackColor = true;
            // 
            // buttonStudentCreate
            // 
            this.buttonStudentCreate.Location = new System.Drawing.Point(17, 143);
            this.buttonStudentCreate.Name = "buttonStudentCreate";
            this.buttonStudentCreate.Size = new System.Drawing.Size(113, 50);
            this.buttonStudentCreate.TabIndex = 2;
            this.buttonStudentCreate.Text = "Create";
            this.buttonStudentCreate.UseVisualStyleBackColor = true;
            this.buttonStudentCreate.Click += new System.EventHandler(this.ButtonStudentCreate_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(412, 351);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(146, 58);
            this.buttonDelete.TabIndex = 3;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // buttonUpdate
            // 
            this.buttonUpdate.Location = new System.Drawing.Point(165, 143);
            this.buttonUpdate.Name = "buttonUpdate";
            this.buttonUpdate.Size = new System.Drawing.Size(113, 50);
            this.buttonUpdate.TabIndex = 4;
            this.buttonUpdate.Text = "Update";
            this.buttonUpdate.UseVisualStyleBackColor = true;
            this.buttonUpdate.Click += new System.EventHandler(this.ButtonUpdate_Click);
            // 
            // textBoxStudentName
            // 
            this.textBoxStudentName.Location = new System.Drawing.Point(70, 40);
            this.textBoxStudentName.Name = "textBoxStudentName";
            this.textBoxStudentName.Size = new System.Drawing.Size(227, 26);
            this.textBoxStudentName.TabIndex = 5;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(70, 72);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(227, 26);
            this.textBox2.TabIndex = 6;
            // 
            // labelStudentName
            // 
            this.labelStudentName.AutoSize = true;
            this.labelStudentName.Location = new System.Drawing.Point(13, 40);
            this.labelStudentName.Name = "labelStudentName";
            this.labelStudentName.Size = new System.Drawing.Size(51, 20);
            this.labelStudentName.TabIndex = 9;
            this.labelStudentName.Text = "Name";
            // 
            // groupBoxStudentCreation
            // 
            this.groupBoxStudentCreation.Controls.Add(this.textBoxStudentName);
            this.groupBoxStudentCreation.Controls.Add(this.labelStudentName);
            this.groupBoxStudentCreation.Controls.Add(this.buttonUpdate);
            this.groupBoxStudentCreation.Controls.Add(this.textBox2);
            this.groupBoxStudentCreation.Controls.Add(this.buttonStudentCreate);
            this.groupBoxStudentCreation.Location = new System.Drawing.Point(57, 40);
            this.groupBoxStudentCreation.Name = "groupBoxStudentCreation";
            this.groupBoxStudentCreation.Size = new System.Drawing.Size(303, 213);
            this.groupBoxStudentCreation.TabIndex = 10;
            this.groupBoxStudentCreation.TabStop = false;
            this.groupBoxStudentCreation.Text = "Student Creation";
            // 
            // dataGridViewStudents
            // 
            this.dataGridViewStudents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewStudents.Location = new System.Drawing.Point(842, 56);
            this.dataGridViewStudents.Name = "dataGridViewStudents";
            this.dataGridViewStudents.RowHeadersWidth = 62;
            this.dataGridViewStudents.RowTemplate.Height = 28;
            this.dataGridViewStudents.Size = new System.Drawing.Size(368, 274);
            this.dataGridViewStudents.TabIndex = 11;
            this.dataGridViewStudents.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewStudents_CellClick);
            this.dataGridViewStudents.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridViewStudents_CellContentClick);
            // 
            // labelGridSuervisor
            // 
            this.labelGridSuervisor.AutoSize = true;
            this.labelGridSuervisor.Location = new System.Drawing.Point(412, 30);
            this.labelGridSuervisor.Name = "labelGridSuervisor";
            this.labelGridSuervisor.Size = new System.Drawing.Size(84, 20);
            this.labelGridSuervisor.TabIndex = 12;
            this.labelGridSuervisor.Text = "Supervisor";
            this.labelGridSuervisor.Click += new System.EventHandler(this.Label1_Click);
            // 
            // labelGridStudent
            // 
            this.labelGridStudent.AutoSize = true;
            this.labelGridStudent.Location = new System.Drawing.Point(842, 29);
            this.labelGridStudent.Name = "labelGridStudent";
            this.labelGridStudent.Size = new System.Drawing.Size(66, 20);
            this.labelGridStudent.TabIndex = 13;
            this.labelGridStudent.Text = "Student";
            // 
            // buttonToJSON
            // 
            this.buttonToJSON.Location = new System.Drawing.Point(290, 259);
            this.buttonToJSON.Name = "buttonToJSON";
            this.buttonToJSON.Size = new System.Drawing.Size(83, 150);
            this.buttonToJSON.TabIndex = 14;
            this.buttonToJSON.Text = "ToJSON";
            this.buttonToJSON.UseVisualStyleBackColor = true;
            this.buttonToJSON.Click += new System.EventHandler(this.ButtonToJSON_Click);
            // 
            // richTextBoxJSON
            // 
            this.richTextBoxJSON.Enabled = false;
            this.richTextBoxJSON.Location = new System.Drawing.Point(57, 259);
            this.richTextBoxJSON.Name = "richTextBoxJSON";
            this.richTextBoxJSON.Size = new System.Drawing.Size(210, 150);
            this.richTextBoxJSON.TabIndex = 15;
            this.richTextBoxJSON.Text = "";
            // 
            // supervisorBindingSource
            // 
            this.supervisorBindingSource.DataSource = typeof(Task19.Model.Supervisor);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1264, 450);
            this.Controls.Add(this.richTextBoxJSON);
            this.Controls.Add(this.buttonToJSON);
            this.Controls.Add(this.labelGridStudent);
            this.Controls.Add(this.labelGridSuervisor);
            this.Controls.Add(this.dataGridViewStudents);
            this.Controls.Add(this.groupBoxStudentCreation);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.buttonRead);
            this.Controls.Add(this.dataGridViewBDBrowser);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBDBrowser)).EndInit();
            this.groupBoxStudentCreation.ResumeLayout(false);
            this.groupBoxStudentCreation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewStudents)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.supervisorBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridViewBDBrowser;
        private System.Windows.Forms.Button buttonRead;
        private System.Windows.Forms.Button buttonStudentCreate;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonUpdate;
        private System.Windows.Forms.TextBox textBoxStudentName;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.BindingSource supervisorBindingSource;
        private System.Windows.Forms.Label labelStudentName;
        private System.Windows.Forms.GroupBox groupBoxStudentCreation;
        private System.Windows.Forms.DataGridView dataGridViewStudents;
        private System.Windows.Forms.Label labelGridSuervisor;
        private System.Windows.Forms.Label labelGridStudent;
        private System.Windows.Forms.Button buttonToJSON;
        private System.Windows.Forms.RichTextBox richTextBoxJSON;
    }
}

