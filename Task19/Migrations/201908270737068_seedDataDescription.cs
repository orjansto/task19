namespace Task19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class seedDataDescription : DbMigration
    {
        public override void Up()
        {   
            Sql("INSERT INTO Supervisors(Name) VALUES('Pedro')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Dean')");
            Sql("INSERT INTO Supervisors(Name) VALUES('Greg')");
        }
        
        public override void Down()
        {
            Sql("DELETE FROM Supervisors WHERE Name='Pedro'");
            Sql("DELETE FROM Supervisors WHERE Name='Dean'");
            Sql("DELETE FROM Supervisors WHERE Name='Greg'");
        }
    }
}
