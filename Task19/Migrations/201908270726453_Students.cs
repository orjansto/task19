namespace Task19.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Students : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Supervisor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Supervisors", t => t.Supervisor_Id)
                .Index(t => t.Supervisor_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "Supervisor_Id", "dbo.Supervisors");
            DropIndex("dbo.Students", new[] { "Supervisor_Id" });
            DropTable("dbo.Students");
        }
    }
}
