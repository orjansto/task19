﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Task19.Model;

namespace Task19
{
    public partial class Form1 : Form
    {
        private slNameDBContext slNameContext;
        int selectedSupervisorKey = 1;
        int selectedStudentKey = 1;
        public Form1()
        {
            InitializeComponent();
        }

        private void Button3_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            slNameContext = new slNameDBContext();
            List<Supervisor> supervisors = slNameContext.Supervisors.ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Name"));
            foreach (Supervisor supervisor in supervisors)
            {
                //    DataGridViewRow row = (DataGridViewRow)dataGridViewBDBrowser.Rows[0].Clone();
                //    row.Cells["Name"].Value = supervisor.Name;
                //    row.Cells["Id"].Value = supervisor.Id;
                //    dataGridViewBDBrowser.Rows.Add(row);


                dt.Rows.Add(supervisor.Id, supervisor.Name);


                //this.dataGridViewBDBrowser.Rows.Insert(0, supervisor);
                ////add each to some component
            }                        dataGridViewBDBrowser.DataSource = dt;
            dataGridViewBDBrowser.Columns["Id"].Width = 20;

            List<Student> students = slNameContext.Students.ToList();
            DataTable dt1 = new DataTable();
            dt1.Columns.Add(new DataColumn("Id"));
            dt1.Columns.Add(new DataColumn("Name"));
            dt1.Columns.Add(new DataColumn("Supervisor"));
            foreach (Student student in students)
            {
                dt1.Rows.Add(student.Id, student.Name, student.Supervisor.Name);
            }

            dataGridViewStudents.DataSource = dt1;
            dataGridViewStudents.Columns["Id"].Width = 20;


            if (int.TryParse(dataGridViewBDBrowser.Rows[0].Cells[0].Value.ToString(), out int key))
            {
                selectedSupervisorKey = key;
            }
            else
            {
                MessageBox.Show("ERROR!!!");
            }
        }

        private void DataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void ButtonUpdate_Click(object sender, EventArgs e)
        {

        }

        private void ButtonStudentCreate_Click(object sender, EventArgs e)
        {

            //int key = 1;//int.Parse(dataGridViewBDBrowser.Rows[dataGridViewBDBrowser.SelectedRows[0].Index].Cells[0].Value.ToString());
            Supervisor aSupervisor = slNameContext.Supervisors.Find(selectedSupervisorKey);
            Student obj = new Model.Student
            {
                Name = textBoxStudentName.Text,
                Supervisor = aSupervisor
            };

            slNameContext.Students.Add(obj);
            slNameContext.SaveChanges();

            MessageBox.Show("Added Student to DB");
            List<Student> students = slNameContext.Students.ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Name"));
            dt.Columns.Add(new DataColumn("Supervisor"));
            foreach (Student student in students)
            {  
                dt.Rows.Add(student.Id, student.Name, student.Supervisor.Name);
            }
            
            dataGridViewStudents.DataSource = dt;
            

        }

        private void DataGridViewBDBrowser_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedSupervisorKey = int.Parse(dataGridViewBDBrowser.Rows[dataGridViewBDBrowser.SelectedCells[0].RowIndex].Cells[0].Value.ToString());
        }

        private void DataGridViewStudents_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            
            slNameContext.Students.Remove(slNameContext.Students.Find(selectedStudentKey));
            slNameContext.SaveChanges();


            MessageBox.Show("Deleted student from DB");
            List<Student> students = slNameContext.Students.ToList();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Id"));
            dt.Columns.Add(new DataColumn("Name"));
            dt.Columns.Add(new DataColumn("Supervisor"));
            foreach (Student student in students)
            {
                dt.Rows.Add(student.Id, student.Name, student.Supervisor.Name);
            }

            dataGridViewStudents.DataSource = dt;


        }

        private void DataGridViewStudents_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedStudentKey = int.Parse(dataGridViewStudents.Rows[dataGridViewStudents.SelectedCells[0].RowIndex].Cells[0].Value.ToString());

        }

        private void ButtonToJSON_Click(object sender, EventArgs e)
        {
            List<Supervisor> supervisors = slNameContext.Supervisors.ToList();
            string JSONString = "";
            foreach (Supervisor supervisor in supervisors)
            {
                
                JSONString += JsonConvert.SerializeObject(supervisor);
            }
            richTextBoxJSON.Text = JSONString;
        }
    }
}
