﻿**Experis Academy, Norway**

**Authors:**
* **Ørjan Storås**

# Experis Week3 Tasks 19, 20 and 21
## Task 19


- [ ] Write a program which allows a university post grad administrator to assign Supervisors (Professors) to Students
to supervise them during their postgrad research degrees
- [x] Create a basic UI for it but **do not wire up any events yet** (name all your components correctly)
- [x] Use a code first workflow to create the initial class for **supervisor** and the corresponding database using entity
framework (DBContext)
- [x] **Do not create the Student class yet!** Only the Supervisor Class with Id and Name




**Step by step tasks**
- [x] Install SQlServer (Express edition 2017)
- [x] Install SQLServer Management Studio
- [x] Create the UI 
- [x] Add Entity Framework Package
- [x] Create our classes (Model)
- [x] Create the DBContext
- [x] Connect to the DB provider
- [x] Execute Migrations

![alt text](https://gitlab.com/orjansto/task19/raw/master/Task19/DBsnip.PNG)

## Task 20

- [x] Add the Student class to you PGM solution with a Supervisor object inside it
- [x] Add the migrations to update the database structure
- [x] Insert some sample data into the supervisor table using a migration
- [x] Populate a ListBox or ComboBox with the Supervisors Names from the database
- [x] Add some textboxes to allow for adding students
- [x] Bind the gridview to the student table
- [x] Use the gridview to allow for Deleting students (Hint DGV allows you to use the cell values)

## Task 21

- [x] Add a button into your PGManagerproject to allow for the current DBSetof Supervisors to be serialized into JSON. 
- [x] You can either write the JSON to a text file within your debug folder and submit the text file or add a component to your UI to display the JSON text
